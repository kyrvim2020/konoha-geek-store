<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package storefront
 */

?>

		</div><!-- .col-full -->
	</div><!-- #content -->

	<?php do_action( 'storefront_before_footer' ); ?>

	<div class="pre-footer"></div>
	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="col-full">

			<?php
			/**
			 * Functions hooked in to storefront_footer action
			 *
			 * @hooked storefront_footer_widgets - 10
			 * @hooked storefront_credit         - 20
			 */
			do_action( 'storefront_footer' );
			?>

		</div><!-- .col-full -->
	</footer><!-- #colophon -->

	<?php do_action( 'storefront_after_footer' ); ?>

</div><!-- #page -->

<script src="https://unpkg.com/swiper/swiper-bundle.js"></script>
<script src="https://unpkg.com/swiper/swiper-bundle.min.js"></script>

<script>
const swiper = new Swiper('.swiper-container', {
	slidesPerView: 4,
	spaceBetween: 50,
	freeMode: true,
	direction: 'horizontal',
	slidesPerGroup: 4,
	loop: true,
	loopFillGroupWithBlank: false,
	navigation: {
		nextEl: '.swiper-cat-next',
		prevEl: '.swiper-cat-prev',
	},

});	

const recentProductsSwiper = new Swiper('.swiper-container.recent', {
	slidesPerView: 4,
	spaceBetween: 50,
	freeMode: true,
	direction: 'horizontal',
	slidesPerGroup: 4,
	loop: true,
	loopFillGroupWithBlank: false,
	navigation: {
		nextEl: '.swiper-recent-next',
		prevEl: '.swiper-recent-prev',
	},

});	
</script>

<?php wp_footer(); ?>

</body>
</html>
