<?php
/**
 * class-groups-ws-cleaner.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-woocommerce
 * @since groups-woocommerce 1.15.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Maintenance - cleans up expired memberships.
 */
class Groups_WS_Cleaner {

	/**
	 * Maximum number of users to check per round.
	 * @var int
	 */
	const USER_DELTA = 100;

	/**
	 * Time to next scheduled cleaning round while not all have been processed.
	 * @var int
	 */
	const DELTA_BUSY = 15;

	/**
	 * Time to next scheduled cleaning round after all have been processed.
	 * @var int
	 */
	const DELTA_IDLE = 86400;

	/**
	 * Adds action on init and scheduled action.
	 */
	public static function init() {
		add_action( 'init', array( __CLASS__, 'wp_init' ), 9999999999 );
		add_action( 'groups_woocommerce_cleaner', array( __CLASS__, 'clean' ), 10, 0 );
	}

	/**
	 * Takes care of scheduling next cleaning round.
	 */
	public static function wp_init() {

		global $wpdb;

		$options = get_option( 'groups-woocommerce', array() );
		$cleaner =
			defined( 'GROUPS_WOOCOMMERCE_CLEANER' ) && GROUPS_WOOCOMMERCE_CLEANER ||
			defined( 'GROUPS_WS_CLEANER' ) && isset( $options[GROUPS_WS_CLEANER] ) && $options[GROUPS_WS_CLEANER] ||
			!isset( $options[GROUPS_WS_CLEANER] ) && defined( 'GROUPS_WS_CLEANER_DEFAULT' ) && GROUPS_WS_CLEANER_DEFAULT;

		$timestamp = wp_next_scheduled( 'groups_woocommerce_cleaner' );

		$delta_busy = intval( defined( 'GROUPS_WOOCOMMERCE_CLEANER_DELTA_BUSY' ) ? GROUPS_WOOCOMMERCE_CLEANER_DELTA_BUSY : self::DELTA_BUSY );
		$delta_idle = intval( defined( 'GROUPS_WOOCOMMERCE_CLEANER_DELTA_IDLE' ) ? GROUPS_WOOCOMMERCE_CLEANER_DELTA_IDLE : self::DELTA_IDLE );

		if ( $timestamp === false && $cleaner ) {
			$status       = get_option( 'groups_woocommerce_cleaner_status' );
			if ( $status !== 'idle' ) {
				$next = $delta_busy;
			} else {
				$next = $delta_idle;
			}
			$next += time();
			$scheduled = wp_schedule_single_event( $next, 'groups_woocommerce_cleaner' );
		}

		if ( $timestamp !== false && !$cleaner ) {
			delete_option( 'groups_woocommerce_cleaner_status' );
			delete_option( 'groups_woocommerce_cleaner_last' );
			wp_unschedule_event( $timestamp, 'groups_woocommerce_cleaner' );
		}
	}

	/**
	 * Scheduled cleaning action.
	 */
	public static function clean() {

		global $wpdb;

		if (
			!class_exists( 'Groups_Group' ) ||
			!class_exists( 'Groups_WS_Terminator' )
		) {
			return;
		}

		set_time_limit( 0 );

		$user_delta = defined( 'GROUPS_WOOCOMMERCE_CLEANER_USER_DELTA' ) ? GROUPS_WOOCOMMERCE_CLEANER_USER_DELTA : self::USER_DELTA;

		$min_user_id = 1 + intval( get_option( 'groups_woocommerce_cleaner_last', 0 ) );
		$max_id = intval( $wpdb->get_var( "SELECT MAX(ID) FROM $wpdb->users" ) );
		$max_user_id = min( $min_user_id + $user_delta, $max_id );

		if ( $max_user_id < $max_id ) {
			update_option( 'groups_woocommerce_cleaner_last', $max_user_id );
			delete_option( 'groups_woocommerce_cleaner_status' );
		} else {
			update_option( 'groups_woocommerce_cleaner_last', 0 );
			update_option( 'groups_woocommerce_cleaner_status', 'idle' );
		}

		$query = $wpdb->prepare(
			"SELECT DISTINCT user_id FROM $wpdb->usermeta WHERE meta_key = '_groups_buckets' AND user_id >= %d AND user_id <= %d",
			intval( $min_user_id ),
			intval( $max_user_id )
		);
		if ( $user_ids = $wpdb->get_col( $query ) ) {

			$registered_group_id = null;
			if ( $registered_group = Groups_Group::read_by_name( Groups_Registered::REGISTERED_GROUP_NAME ) ) {
				$registered_group_id = $registered_group->group_id;
			}

			if ( is_array( $user_ids ) ) {
				foreach ( $user_ids as $user_id ) {
					$buckets = get_user_meta( $user_id, '_groups_buckets', true );
					if ( is_array( $buckets ) ) {
						foreach ( $buckets as $group_id => $timestamps ) {
							$group = Groups_Group::read( $group_id );
							if ( $group ) {
								if ( $group->group_id != $registered_group_id ) {
									$max_timestamp = -1;
									foreach ( $timestamps as $timestamp ) {
										$max_timestamp = max( $max_timestamp, $timestamp );
									}
									if ( $max_timestamp == Groups_WS_Terminator::ETERNITY ) {
									} else {
										$now = time();
										if ( count( $timestamps ) < 1) {
										} else if ( $max_timestamp < $now ) {
											self::maybe_delete( $user_id, $group_id );
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}

	/**
	 * Terminates the membership if no valid orders grant access.
	 *
	 * @param int $user_id
	 * @param int $group_id
	 */
	public static function maybe_delete( $user_id, $group_id ) {
		$order_ids = Groups_WS_Handler::get_valid_order_ids_granting_group_membership_from_order_items( $user_id, $group_id );
		$sub_ids = Groups_WS_Handler::get_valid_subscription_ids_granting_group_membership( $user_id, $group_id );
		if ( ( count( $order_ids ) == 0 ) && ( count( $sub_ids ) == 0 ) ) {
			if ( GROUPS_WS_LOG ) {
				error_log( sprintf( __METHOD__ . ' cleaner attempting to remove membership for user ID %d with group ID %d', $user_id, $group_id ) );
			}
			Groups_WS_Terminator::terminate_membership( $user_id, $group_id );
		}
	}
}
Groups_WS_Cleaner::init();
