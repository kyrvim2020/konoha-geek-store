<?php
/**
 * class-groups-ws-admin.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-woocommerce
 * @since groups-woocommerce 1.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Admin section for Groups integration.
 */
class Groups_WS_Admin {

	const NONCE                   = 'groups-woocommerce-admin-nonce';
	const MEMBERSHIP_ORDER_STATUS = GROUPS_WS_MEMBERSHIP_ORDER_STATUS;
	const SHOW_DURATION           = GROUPS_WS_SHOW_DURATION;
	const FORCE_REGISTRATION      = GROUPS_WS_FORCE_REGISTRATION;
	const SHOW_IN_USER_PROFILE    = GROUPS_WS_SHOW_IN_USER_PROFILE;
	const SHOW_IN_EDIT_PROFILE    = GROUPS_WS_SHOW_IN_EDIT_PROFILE;
	const CLEANER                 = GROUPS_WS_CLEANER;
	const DISMISS_EXTENSIONS      = 'gw-dismiss-extensions';
	const DISMISS_META_KEY        = 'groups-woocommerce-hide-extensions';

	/**
	 * Admin setup.
	 */
	public static function init() {
		add_action( 'admin_menu', array( __CLASS__, 'admin_menu' ), 40 );
		add_action( 'admin_init', array( __CLASS__, 'admin_init' ) );
	}

	/**
	 * Adds the admin section.
	 */
	public static function admin_menu() {
		$admin_page = add_submenu_page(
			'woocommerce',
			__( 'Groups', 'groups-woocommerce' ),
			__( 'Groups', 'groups-woocommerce' ),
			GROUPS_ADMINISTER_OPTIONS,
			'groups_woocommerce',
			array( __CLASS__, 'groups_woocommerce' )
		);
// 		add_action( 'admin_print_scripts-' . $admin_page, array( __CLASS__, 'admin_print_scripts' ) );
// 		add_action( 'admin_print_styles-' . $admin_page, array( __CLASS__, 'admin_print_styles' ) );
	}

	public static function admin_init() {
		if ( !empty( $_GET[self::DISMISS_EXTENSIONS] ) && wp_verify_nonce( $_GET[self::NONCE], 'hide' ) ) {
			$user_id = get_current_user_id();
			add_user_meta( $user_id, self::DISMISS_META_KEY, 'yes', true );
		}
	}

	/**
	 * Renders the admin section.
	 */
	public static function groups_woocommerce() {

		if ( !current_user_can( GROUPS_ADMINISTER_OPTIONS ) ) {
			wp_die( __( 'Access denied.', 'groups-woocommerce' ) );
		}

		$options = get_option( 'groups-woocommerce', null );
		if ( $options === null ) {
			if ( add_option( 'groups-woocommerce', array(), null, 'no' ) ) {
				$options = get_option( 'groups-woocommerce' );
			}
		}

		if ( isset( $_POST['submit'] ) ) {
			if ( wp_verify_nonce( $_POST[self::NONCE], 'set' ) ) {
				$order_status = isset( $_POST[self::MEMBERSHIP_ORDER_STATUS] ) ? $_POST[self::MEMBERSHIP_ORDER_STATUS] : 'completed';
				switch ( $order_status ) {
					case 'completed' :
					case 'processing' :
						break;
					default :
						$order_status = GROUPS_WS_DEFAULT_MEMBERSHIP_ORDER_STATUS;
				}
				$options[self::MEMBERSHIP_ORDER_STATUS] = $order_status;
				$options[GROUPS_WS_REMOVE_ON_HOLD]      = isset( $_POST[GROUPS_WS_REMOVE_ON_HOLD] );
				$options[self::SHOW_DURATION]           = isset( $_POST[self::SHOW_DURATION] );
				$options[self::FORCE_REGISTRATION]      = isset( $_POST[self::FORCE_REGISTRATION] );
				$options[self::SHOW_IN_USER_PROFILE]    = isset( $_POST[self::SHOW_IN_USER_PROFILE] );
				$options[self::SHOW_IN_EDIT_PROFILE]    = isset( $_POST[self::SHOW_IN_EDIT_PROFILE] );
				$options[self::CLEANER]                 = isset( $_POST[self::CLEANER] );

				update_option( 'groups-woocommerce', $options );
			}
		}

		$order_status         = isset( $options[self::MEMBERSHIP_ORDER_STATUS] ) ? $options[self::MEMBERSHIP_ORDER_STATUS] : GROUPS_WS_DEFAULT_MEMBERSHIP_ORDER_STATUS;
		$remove_on_hold       = isset( $options[GROUPS_WS_REMOVE_ON_HOLD] ) ? $options[GROUPS_WS_REMOVE_ON_HOLD] : GROUPS_WS_DEFAULT_REMOVE_ON_HOLD;
		$show_duration        = isset( $options[self::SHOW_DURATION] ) ? $options[self::SHOW_DURATION] : GROUPS_WS_DEFAULT_SHOW_DURATION;
		$force_registration   = isset( $options[self::FORCE_REGISTRATION] ) ? $options[self::FORCE_REGISTRATION] : GROUPS_WS_DEFAULT_FORCE_REGISTRATION;
		$show_in_user_profile = isset( $options[self::SHOW_IN_USER_PROFILE] ) ? $options[self::SHOW_IN_USER_PROFILE] : GROUPS_WS_DEFAULT_SHOW_IN_USER_PROFILE;
		$show_in_edit_profile = isset( $options[self::SHOW_IN_EDIT_PROFILE] ) ? $options[self::SHOW_IN_EDIT_PROFILE] : GROUPS_WS_DEFAULT_SHOW_IN_EDIT_PROFILE;
		$cleaner              = isset( $options[self::CLEANER] ) ? $options[self::CLEANER] : GROUPS_WS_CLEANER_DEFAULT;

		$cleaner_timestamp    = wp_next_scheduled( 'groups_woocommerce_cleaner' );
		$cleaner_last         = get_option( 'groups_woocommerce_cleaner_last', null );
		$cleaner_status       = get_option( 'groups_woocommerce_cleaner_status', null );

		echo '<style type="text/css">';
		echo 'h2 { border-bottom: 2px solid #ccc; margin: 16px 0; padding: 8px 0; width: 98%; }';
		echo '</style>';

		echo '<div class="groups-woocommerce">';

		echo '<h1>' . __( 'Groups', 'groups-woocommerce' ) . '</h1>';

		echo
			'<form action="" name="options" method="post">' .
			'<div>' .

			'<h2>' . __( 'Group membership', 'groups-woocommerce' ) . '</h2>' .
			'<h3>' . __( 'Order Status', 'groups-woocommerce' ) . '</h3>' .
			'<p>' .
			'<label>' . __( 'Add users to or remove from groups as early as the order is ...', 'groups-woocommerce' ) .
			' ' .
			'<select name="' . self::MEMBERSHIP_ORDER_STATUS . '">' .
			'<option value="completed" ' . ( $order_status == 'completed' ? ' selected="selected" ' : '' ) . '>' . __( 'Completed', 'groups-woocommerce' ) . '</option>' .
			'<option value="processing" ' . ( $order_status == 'processing' ? ' selected="selected" ' : '' ) . '>' . __( 'Processing', 'groups-woocommerce' ) . '</option>' .
			'</select>' .
			'</label>' .
			'</p>' .
			'<p class="description">' . __( 'Note that users will always be added to or removed from groups when an order is completed.', 'groups-woocommerce' ) . '</p>' .
			'<p class="description">' . __( 'This setting does not apply to subscriptions.', 'groups-woocommerce' ) . '</p>' .

			'<h3>' . __( 'Subscription Status', 'groups-woocommerce' ) . '</h3>' .
			'<p>' .
			'<label>' .
			sprintf( '<input name="%s" type="checkbox" %s />', GROUPS_WS_REMOVE_ON_HOLD, $remove_on_hold ? ' checked="checked" ' : '' ) .
			' ' .
			__( 'Remove users from groups when a subscription is on hold; add them back when a subscription is reactivated.', 'groups-woocommerce' ) .
			'</label>' .
			'</p>' .
			'<p class="description">' . __( 'This setting only applies to subscriptions.', 'groups-woocommerce' ) . '</p>' .

			'<h2>' . __( 'Durations', 'groups-woocommerce' ) . '</h2>' .
			'<p>' .
			'<label>' .
			sprintf( '<input name="show_duration" type="checkbox" %s />', $show_duration ? ' checked="checked" ' : '' ) .
			' ' .
			__( 'Show durations', 'groups-woocommerce' ) .
			'</label>' .
			'</p>' .
			'<p class="description">' . __( 'Modifies the way product prices are displayed to show durations.', 'groups-woocommerce' ) . '</p>' .

			'<h2>' . __( 'Force registration', 'groups-woocommerce' ) . '</h2>' .
			'<p>' .
			'<label>' .
			'<input type="checkbox" ' . ( $force_registration ? ' checked="checked" ' : '' ) . ' name="' . GROUPS_WS_FORCE_REGISTRATION . '" />' .
			'&nbsp;' .
			__( 'Force registration on checkout', 'groups-woocommerce' ) .
			'</label>' .
			'</p>' .
			'<p class="description">' .
			__( 'Force registration on checkout when a subscription or a product which relates to groups is in the cart. The login form will also be shown to allow existing users to log in.', 'groups-woocommerce' ) .
			'</p>' .

			'<h2>' . __( 'User profiles', 'groups-woocommerce' ) . '</h2>' .
			'<p>' .
			'<label>' .
			'<input type="checkbox" ' . ( $show_in_user_profile ? ' checked="checked" ' : '' ) . ' name="' . self::SHOW_IN_USER_PROFILE . '" />' .
			'&nbsp;' .
			__( 'Show membership info in user profiles', 'groups-woocommerce' ) .
			'</label>' .
			'</p>' .
			'<p class="description">' .
			__( 'If enabled, users can see information about their group memberships on the profile page.', 'groups-woocommerce' ) .
			'</p>' .
			'<p>' .
			'<label>' .
			'<input type="checkbox" ' . ( $show_in_edit_profile ? ' checked="checked" ' : '' ) . ' name="' . self::SHOW_IN_EDIT_PROFILE . '" />' .
			'&nbsp;' .
			__( 'Show membership info when editing user profiles', 'groups-woocommerce' ) .
			'</label>' .
			'</p>' .
			'<p class="description">' .
			__( 'If enabled, users who can edit other users can see and modify group memberships.', 'groups-woocommerce' ) .
			'</p>' .

			'<h2>' . __( 'Maintenance', 'groups-woocommerce' ) . '</h2>' .
			'<p>' .
			'<label>' .
			sprintf( '<input name="%s" type="checkbox" %s />', GROUPS_WS_CLEANER, $cleaner ? ' checked="checked" ' : '' ) .
			' ' .
			__( 'Clean up expired memberships', 'groups-woocommerce' ) .
			'</label>' .
			'</p>' .
			'<p class="description">' . __( 'This setting does not apply to subscriptions.', 'groups-woocommerce' ) . '</p>' .
			'<p>' .
			sprintf(
				'<em>%s &rarr; %s [#%s] {%s}</em>',
				esc_html__( 'Status', 'groups-woocommerce' ),
				( $cleaner_timestamp !== false ? date_i18n( 'Y-m-d H:i:s', $cleaner_timestamp ) : '&mdash;' ),
				( $cleaner_last !== null ? intval( $cleaner_last ) : '&mdash;' ),
				( $cleaner_status !== null ? esc_html( $cleaner_status ) : '&mdash;' )
			) .
			'</p>' .

			'<p style="border-top:1px solid #e0e0e0;padding-top:10px;margin-top:10px; width:98%;">' .
			wp_nonce_field( 'set', self::NONCE, true, false ) .
			'<input class="button button-primary" type="submit" name="submit" value="' . __( 'Save', 'groups-woocommerce' ) . '"/>' .
			'</p>' .
			'</div>' .
			'</form>';

		echo '</div>'; // .groups-woocommerce

		echo self::extensions();

		if ( GROUPS_WS_LOG ) {
			$crons = _get_cron_array();
			echo '<h2>Cron</h2>';
			echo '<pre>';
			echo var_export( $crons, true );
			echo '</pre>';
		}
	}

	/**
	 * Returns extensions output.
	 *
	 * @return string
	 */
	public static function extensions() {

		global $groups_woocommerce_extensions;

		$output = '';

		$user_id = get_current_user_id();
		$dismiss = get_user_meta( $user_id, self::DISMISS_META_KEY, true );

		if ( $dismiss !== 'yes' && !isset( $groups_woocommerce_extensions ) ) {

			$groups_woocommerce_extensions = true;

			$output .= '<style type="text/css">';

			$output .= '.groups-woocommerce-extensions-container {';
			$output .= 'overflow: hidden;';
			$output .= 'position: relative;';
			$output .= '}';
			$output .= '.groups-woocommerce-extensions {';
			$output .= 'display: flex; flex-wrap: wrap; position: relative;';
			$output .= '}';
			$output .= '.groups-woocommerce-extension-container {';
			$output .= 'flex: 1; margin: 0.62em;';
			$output .= '}';
			$output .= '.groups-woocommerce-extension-container.featured {';
			$output .= 'flex: 2;';
			$output .= '}';
			$output .= '.groups-woocommerce-extension-container a {';
			$output .= 'padding: 1em; margin: 0.62em; display: block; border: 1px solid #ccc; text-align: center; border-radius: 5px; text-decoration: none; color: #666; background-color: #fff;';
			$output .= '}';
			$output .= 'div.groups-woocommerce-extension-container .extension-title {';
			$output .= 'color: #a64c84; display: block; font-size: 1.2em; font-weight: 700; line-height: 1.22em; border-bottom: 1px solid #ccc; margin-bottom: 0.31em; padding-bottom: 0.31em;';
			$output .= '}';
			$output .= 'div.groups-woocommerce-extension-container.featured .extension-title {';
			$output .= 'color: #a64c84; display: block; font-size: 1.6em; font-weight: 900; line-height: 1.62em;';
			$output .= '}';
			$output .= 'div.groups-woocommerce-extension-container .extension-description {';
			$output .= 'display: block; padding: 0.6em;';
			$output .= '}';

			$output .= '.groups-woocommerce-extensions-container a.woocommerce-message-close::before {';
			$output .= 'position: relative;';
			$output .= 'top: 18px;';
			$output .= 'left: -20px;';
			$output .= '-webkit-transition: all .1s ease-in-out;';
			$output .= 'transition: all .1s ease-in-out;';
			$output .= '}';
			$output .= '.groups-woocommerce-extensions-container a.woocommerce-message-close {';
			$output .= 'position: static;';
			$output .= 'float: right;';
			$output .= 'top: 0;';
			$output .= 'right: 0;';
			$output .= 'padding: 0 15px 10px 28px;';
			$output .= 'margin-top: -10px;';
			$output .= 'font-size: 13px;';
			$output .= 'line-height: 1.23076923;';
			$output .= 'text-decoration: none;';
			$output .= '}';

			$output .= '</style>';

			$extensions = array(
				array(
					'title'       => 'Group Coupons',
					'description' => esc_html__( 'Offer exclusive, automatic and targeted coupon discounts for your customers! Use group memberships and roles to control the validity of coupons.', 'groups-woocommerce' ),
					'url'         => 'https://woocommerce.com/products/group-coupons/',
					'featured'    => true
				),
				array(
					'title'       => 'WooCommerce Product Search',
					'description' => esc_html__( 'The essential extension for every WooCommerce store! The perfect Search Engine for your store helps your customers to find and buy the right products quickly.', 'groups-woocommerce' ),
					'url'         => 'https://woocommerce.com/products/woocommerce-product-search/',
					'featured'    => true
				),
				array(
					'title'       => 'Sales Analysis',
					'description' => esc_html__( 'Sales Analysis oriented at Marketing & Management. Get in-depth views on fundamental Business Intelligence, focused on Sales and net Revenue Trends, International Sales Reports, Product Market and Customer Trends.', 'groups-woocommerce' ),
					'url'         => 'https://woocommerce.com/products/sales-analysis-for-woocommerce/',
					'featured'    => true
				)
			);

			$output .= '<div class="groups-woocommerce-extensions-container">';

			// dismiss
			$current_url = ( is_ssl() ? 'https://' : 'http://' ) . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			$hide_url = wp_nonce_url(
				add_query_arg( self::DISMISS_EXTENSIONS, true, $current_url ),
				'hide',
				self::NONCE
			);
			$output .= sprintf(
				'<a class="woocommerce-message-close notice-dismiss" href="%s">%s</a>',
				esc_url( $hide_url ),
				esc_html__( 'Dismiss', 'woocommerce-coupon-shortcodes' )
			);

			// content
			$output .= '<h2 style="padding-top: 0.62em">' . esc_html__( 'Related Extensions', 'groups-woocommerce' ) . '</h2>';

			$output .= '<div class="options_group">';

			$output .= '<p>';
			$output .= esc_html__( 'These related extensions help to improve your store &hellip;', 'groups-woocommerce' );
			$output .= '</p>';

			$output .= '<div class="groups-woocommerce-extensions">';

			foreach ( $extensions as $ext ) {
				$output .= sprintf(
					'<div class="groups-woocommerce-extension-container %s">',
					isset( $ext['featured'] ) && $ext['featured'] ? 'featured' : ''
				);
				$output .= sprintf(
					'<a target="_blank" href="%s"><div class="extension-title">%s</div><div class="extension-description">%s</div></a>',
					$ext['url'],
					$ext['title'],
					$ext['description']
				);
				$output .= '</div>';
			}

			$output .= '</div>'; // .groups-woocommerce-extensions
			$output .= '</div>'; // .options_group
			$output .= '</div>'; // .groups-woocommerce-extensions-container
		}
		return $output;
	}
}
Groups_WS_Admin::init();
