<?php
/**
 * class-groups-ws-admin-product.php
 *
 * Copyright (c) "kento" Karim Rahimpur www.itthinx.com
 *
 * This code is provided subject to the license granted.
 * Unauthorized use and distribution is prohibited.
 * See COPYRIGHT.txt and LICENSE.txt
 *
 * This code is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 *
 * This header and all notices must be kept intact.
 *
 * @author Karim Rahimpur
 * @package groups-woocommerce
 * @since groups-woocommerce 1.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Admin extensions to the product post type.
 */
class Groups_WS_Admin_Product {

	/**
	 * Hooks.
	 */
	public static function init() {
		if ( is_admin() ) { // yeah yeah ... just in case
			add_filter( 'manage_product_posts_columns', array( __CLASS__, 'manage_product_posts_columns' ) );
			add_action( 'manage_product_posts_custom_column', array( __CLASS__, 'manage_product_posts_custom_column' ), 10, 2 );
			add_filter( 'manage_edit-product_sortable_columns', array( __CLASS__, 'manage_edit_product_sortable_columns' ) );
			// @deprecated add_action( 'admin_init', array( __CLASS__, 'admin_init' ) );

			// @todo filter by groups added / removed
			// add_filter( 'posts_where', array( __CLASS__, 'posts_where' ), 10, 2 );
			add_filter( 'posts_join', array( __CLASS__, 'posts_join' ), 10, 2 );
			add_filter( 'posts_orderby', array( __CLASS__, 'posts_orderby' ), 10, 2 );
		}
	}

	/**
	 * Sorting is done with posts_join and posts_orderby instead.
	 *
	 * @deprecated
	 */
	public static function admin_init() {
		$screen = get_current_screen();
		if ( isset( $screen->id ) && ( $screen->id == 'edit-product' ) ) {
			add_filter( 'request', array( __CLASS__, 'request' ) );
		}
	}

	/**
	 * Modify the $query_vars to sort by meta, but this is useless because
	 * we need to sort by the content of the related group names list.
	 *
	 * Using posts_join and posts_orderby filters instead.
	 *
	 * @deprecated
	 *
	 * @param array $query_vars
	 *
	 * @return array
	 */
	public static function request( $query_vars ) {
		if ( !isset( $query_vars['orderby'] ) || ( isset( $query_vars['orderby'] ) && 'groups-wc' == $query_vars['orderby'] ) ) {
			$query_vars = array_merge( $query_vars, array(
				'meta_key' => '_groups_groups',
				'orderby' => 'meta_value'
			) );
		}
		return $query_vars;
	}

	/**
	 * Add the Groups column to the product overview screen.
	 *
	 * @param array $posts_columns
	 *
	 * @return array
	 */
	public static function manage_product_posts_columns( $posts_columns ) {
		$posts_columns['groups-wc'] = sprintf(
			'<span title="%s">%s</span>',
			__( 'Groups to which customer is added and removed (within parenthesis).', 'groups-woocommerce' ),
			__( 'Groups', 'groups-woocommerce' )
		);
		return $posts_columns;
	}

	/**
	 * Adds sortable columns.
	 *
	 * @param array $posts_columns
	 *
	 * @return array
	 */
	public static function manage_edit_product_sortable_columns( $posts_columns ) {
		$posts_columns['groups-wc'] = 'groups-wc';
		return $posts_columns;
	}

	/**
	 * Renders the additional Groups column's content for each entry.
	 *
	 * @param string $column_name
	 * @param int $post_id
	 */
	public static function manage_product_posts_custom_column( $column_name, $post_id ) {
		switch ( $column_name ) {
			case 'groups-wc' :
				echo self::render_group_list( $post_id );
				break;

		}
	}

	/**
	 * Render the list of groups related to a product - includes groups to which
	 * the user is added and those from which the user is removed.
	 *
	 * @param int $post_id Product's post ID
	 *
	 * @return string
	 */
	public static function render_group_list( $post_id ) {

		global $wpdb;

		$output = '';

		$product_groups        = get_post_meta( $post_id, '_groups_groups', false );
		$product_groups_remove = get_post_meta( $post_id, '_groups_groups_remove', false );

		$group_table = _groups_get_tablename( 'group' );
		$groups = $wpdb->get_results( "SELECT * FROM $group_table ORDER BY name" );

		if ( count( $groups ) > 0 ) {
			$list = array();
			foreach( $groups as $group ) {
				if ( is_array( $product_groups ) && in_array( $group->group_id, $product_groups ) ) {
					$list[] = wp_filter_nohtml_kses( $group->name );
				}
				if ( is_array( $product_groups_remove ) && in_array( $group->group_id, $product_groups_remove ) ) {
					$list[] = '(' . wp_filter_nohtml_kses( $group->name ) . ')';
				}
			}
			$output .= implode( ', ', $list );
		}

		return $output;
	}

	/**
	 * Enhance the orderby clause.
	 *
	 * @param string $orderby
	 * @param WP_Query $query
	 *
	 * @return string
	 */
	public static function posts_orderby( $orderby, $query ) {
		if ( self::use_ordering() ) {
			switch ( $query->get( 'order' ) ) {
				case 'desc' :
				case 'DESC' :
					$order = 'DESC';
					break;
				default :
					$order = 'ASC';
			}
			$prefix = ' groups_wc.groups ' . $order;
			if ( !empty( $orderby ) ) {
				$prefix .= ' , ';
			}
			$orderby = $prefix . $orderby;
		}
		return $orderby;
	}

	/**
	 * Enhance the join clause.
	 *
	 * @param string $join
	 * @param WP_Query $query
	 *
	 * @return string
	 */
	public static function posts_join( $join, $query ) {
		global $wpdb;
		if ( self::use_ordering() ) {
			$group_table = _groups_get_tablename( 'group' );
			// @todo enhance the query to take removed groups into account; consider using variations' info too
			$join .=
				" " .
				"LEFT JOIN ( " .
					"SELECT p.ID post_id, GROUP_CONCAT( DISTINCT _groups_groups.group_name ORDER BY _groups_groups.group_name ASC ) groups " .
					"FROM $wpdb->posts p " .
					"LEFT JOIN (" .
						"SELECT post_id, g.name group_name " .
						"FROM $wpdb->postmeta pm " .
						"LEFT JOIN $group_table g ON pm.meta_value = g.group_id " .
						"WHERE pm.meta_key = '_groups_groups' " .
					") AS _groups_groups ON p.ID = _groups_groups.post_id GROUP BY p.ID " .
				") groups_wc ON $wpdb->posts.ID = groups_wc.post_id ";
		}
		return $join;
	}

	/**
	 * Whether to add our ordering by groups.
	 *
	 * @return boolean
	 */
	private static function use_ordering() {
		return is_admin() && !empty( $_REQUEST['orderby'] ) && $_REQUEST['orderby'] === 'groups-wc';
	}
}
Groups_WS_Admin_Product::init();
