=== Groups WooCommerce ===
Contributors: itthinx
Tags: group, groups, member, members, membership, memberships, woocommerce
Requires at least: 5.5
Requires PHP: 5.6.0
Tested up to: 5.8
Stable tag: 1.24.0

Memberships with Groups and WooCommerce. Integrates Groups with WooCommerce and WooCommerce Subscriptions for group membership management based on product purchases and subscriptions.

== Description ==

This extension integrates [WooCommerce](http://wordpress.org/extend/plugins/woocommerce) and [Groups](https://wordpress.org/plugins/groups/).

== Installation ==

1. Go to *Plugins > Add New > Upload* and choose the plugin's zip file, or extract the contents and copy the `groups-woocommerce` folder to your site's `/wp-content/plugins/` directory.
2. Enable the plugin from the *Plugins* menu in WordPress.

== Frequently Asked Questions ==

= Where is the documentation? =

Please go to the [Documentation](http://docs.woocommerce.com/document/groups-woocommerce/) page.

= I have a question, where do I ask? =

Please go to the [Support](http://woocommerce.com/my-account/tickets) page.

You can also leave a comment at the author's [Groups WooCommerce](http://www.itthinx.com/plugins/groups-woocommerce/) plugin page.

== Screenshots ==

Please visit the [documentation](http://docs.woocommerce.com/document/groups-woocommerce/) page.

== Changelog ==

Please refer to the changelog.txt included in the plugin's root folder.

== Upgrade Notice ==

This version has been updated for compatibility with the latest versions of WordPress and WooCommerce.

See the changelog.txt for details.
